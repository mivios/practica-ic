﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.Celulares
{
    public class CelularesDetalleRepositorio : Repositorio<Detalle>
    {
        /// <summary>
        /// Contructor inicializador de repositorio base
        /// </summary>
        /// <param name="contexto"></param>
        public CelularesDetalleRepositorio(Contexto contexto) : base(contexto) { }

        /// <summary>
        /// Guardar lista importada de detalles
        /// </summary>
        /// <param name="lst"></param>
        /// <returns></returns>
        public bool GuardarDetalle(List<Detalle> lst)
        {
            foreach (var item in lst)
            {
                var busqueda = Contexto.ListaCelulares.Where(x => x.MobileLine == item.MobileLine).FirstOrDefault();
                if (busqueda != null)
                {
                    Guardar(item);
                }
            }
            Contexto.SaveChanges();
            return true;
        }
    }
}
