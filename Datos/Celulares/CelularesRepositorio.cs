﻿using System.Collections.Generic;
using System.Linq;

namespace Datos.Celulares
{
    public class CelularesRepositorio : Repositorio<ListaCelulares>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="contexto"></param>
        public CelularesRepositorio(Contexto contexto) : base(contexto) { }

        /// <summary>
        /// Listado de celulares
        /// </summary>
        /// <returns></returns>
        public List<ListaCelulares> ListaCelulares()
        {
            return (from a in Contexto.ListaCelulares                    
                    select a).ToList();
        }

        /// <summary>
        /// Detalles de listado de celular
        /// </summary>
        /// <param name="idCelular"></param>
        /// <returns></returns>
        public ListaCelulares ListaCelularesDetalle(int idCelular)
        {
            var r = (from a in Contexto.ListaCelulares
                     where a.MobileLineId == idCelular
                     select a).FirstOrDefault();
            return r;
        }

        /// <summary>
        /// Guardar lista importada
        /// </summary>
        /// <param name="lst">lista mapeada</param>
        /// <returns></returns>
        public bool GuardarLista(List<ListaCelulares> lst)
        {
            foreach (var item in lst)
            {
                var busqueda = Contexto.ListaCelulares.Where(x => x.MobileLine == item.MobileLine);
                if (busqueda.Count() == 0)
                {
                    Guardar(item);
                }
            }
            Contexto.SaveChanges();
            return true;
        }
    }
}
