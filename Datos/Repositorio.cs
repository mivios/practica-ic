﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos
{
    public class Repositorio<TEntity> where TEntity : class
    {
        protected readonly Contexto Contexto;

        protected Repositorio(Contexto contexto)
        {
            Contexto = contexto;
        }

        /// <summary>
        /// Metodo generico para guardar una entidad
        /// </summary>
        /// <param name="entidad"></param>
        public virtual void Guardar(TEntity entidad)
        {
            Contexto.Set<TEntity>().Add(entidad);
        }

        public virtual void Modificar(TEntity entidad)
        {
            Contexto.Set<TEntity>().Attach(entidad);
            Contexto.Entry(entidad).State = EntityState.Modified;
        }

        public void Eliminar(TEntity entidad)
        {
            Contexto.Set<TEntity>().Attach(entidad);
            Contexto.Set<TEntity>().Remove(entidad);
        }
    }
}
