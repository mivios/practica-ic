﻿using System.Collections.Generic;
using System.Linq;

namespace Datos.Usuarios
{
    public class UsuarioRepositorio : Repositorio<Usuario>
    {
        /// <summary>
        /// Constructor utilizando repositorio base
        /// </summary>
        /// <param name="contexto">BD</param>
        public UsuarioRepositorio(Contexto contexto) : base(contexto) { }

        /// <summary>
        /// Listado de usuarios
        /// </summary>
        /// <returns></returns>
        public List<spdObtenerUsuarios_Result> ListaUsuarios()
        {
            var spResult = Contexto.spdObtenerUsuarios().ToList();
            return spResult;
        }

        /// <summary>
        /// Guardar Usuario
        /// </summary>
        /// <param name="usr">modelo de contexto</param>
        public void GuardarUsuario(Usuario usr)
        {
            Guardar(usr);
            Contexto.SaveChanges();
        }

        /// <summary>
        /// Modificar usuario
        /// </summary>
        /// <param name="usr">modelo de contexto</param>
        public void ModificarUsuario(Usuario usr)
        {
            Modificar(usr);
            Contexto.SaveChanges();
        }

        /// <summary>
        /// Eliminar usuario
        /// </summary>
        /// <param name="idUsuario">identificador</param>
        public void EliminarUsuario(int idUsuario)
        {
            Eliminar(Contexto.Usuario.Find(idUsuario));
            Contexto.SaveChanges();
        }

        /// <summary>
        /// Detalles de registro
        /// </summary>
        /// <param name="idUsuario">identificador</param>
        /// <returns></returns>
        public Usuario ObtenerUsuario(int idUsuario)
        {
            var usr = Contexto.Usuario.Find(idUsuario);
            if (usr == null)
                return new Usuario();
            return usr;

        }
    }
}
