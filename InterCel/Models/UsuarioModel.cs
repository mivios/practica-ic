﻿using Datos;
using Datos.Usuarios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Web;

namespace InterCel.Models
{
    public class UsuarioModel
    {
        #region variables
        public int IDUsuario { get; set; }
        public int IDTipoUsuario { get; set; }
        public string TipoUsuario { get; set; }
        public int IDRol { get; set; }
        public string Rol { get; set; }
        public string Nombre { get; set; }
        public string Login { get; set; }
        public string Contrasena { get; set; }
        public List<UsuarioModel> Usuarios { get; set; }
        public UsuarioModel Usuario { get; set; }
        #endregion

        /// <summary>
        /// Obtener listado de usuarios
        /// </summary>
        /// <returns></returns>
        public List<UsuarioModel> ObtenerUsuarios()
        {
            using (var bd = new Contexto())
            {
                List<UsuarioModel> listaMapeada = new List<UsuarioModel>();
                var repoUsuarios = new UsuarioRepositorio(bd);

                foreach (var item in repoUsuarios.ListaUsuarios())
                {
                    var temp = new UsuarioModel
                    {
                        IDUsuario = item.IDUsuario,
                        TipoUsuario = item.TipoUsuario,
                        Rol = item.Rol,
                        Nombre = item.Nombre,
                        Login = item.Login,
                        Contrasena = item.Contrasena
                    };
                    listaMapeada.Add(temp);
                }
                return listaMapeada;
            }
        }

        /// <summary>
        /// Obtener detalles de usuario
        /// </summary>
        /// <param name="idUsuario">identificador de usuario</param>
        /// <returns></returns>
        public UsuarioModel ObtenerUsuario(int idUsuario)
        {
            using (var bd = new Contexto())
            {
                var repoUsuario = new UsuarioRepositorio(bd);
                var res = repoUsuario.ObtenerUsuario(idUsuario);
                return new UsuarioModel
                {
                    IDUsuario = res.IDUsuario,
                    IDTipoUsuario = res.IDTipoUsuario,
                    IDRol = res.IDRol,
                    Nombre = res.Nombre,
                    Login = res.Login,
                    Contrasena = res.Contrasena
                };
            }
        }

        /// <summary>
        /// Guardar usuario
        /// </summary>
        /// <param name="usr">modeloUsuario</param>
        public void Guardar(UsuarioModel usr)
        {
            using (var bd = new Contexto())
            {
                var repoUsuario = new UsuarioRepositorio(bd);
                var modeloGuardar = new Usuario
                {
                    IDTipoUsuario = usr.IDTipoUsuario,
                    IDRol = usr.IDRol,
                    Nombre = usr.Nombre,
                    Login = usr.Login,
                    Contrasena = usr.Contrasena
                };
                repoUsuario.GuardarUsuario(modeloGuardar);
            }
        }

        /// <summary>
        /// Modificar usuario
        /// </summary>
        /// <param name="usr">modeloUsuario modificado</param>
        public void Modificar(UsuarioModel usr)
        {
            using (var bd = new Contexto())
            {
                var repoUsuario = new UsuarioRepositorio(bd);
                var user = bd.Usuario.Find(usr.IDUsuario);

                if(user != null)
                {
                    user.IDTipoUsuario = usr.IDTipoUsuario;
                    user.IDRol = usr.IDRol;
                    user.Nombre = usr.Nombre;
                    user.Login = usr.Login;
                    user.Contrasena = usr.Contrasena;
                }
                repoUsuario.ModificarUsuario(user);
            }
        }

        /// <summary>
        /// Eliminar usuario
        /// </summary>
        /// <param name="idUsuario">identificadir de usuario</param>
        public void Eliminar(int idUsuario)
        {
            using (var bd = new Contexto())
            {
                var repoUsuario = new UsuarioRepositorio(bd);
                repoUsuario.EliminarUsuario(idUsuario);
            }
        }
    }
}