﻿using Datos;
using Datos.Celulares;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InterCel.Models
{
    public class LstCelularModel
    {
        /// <summary>
        /// Contructor Modelo
        /// </summary>
        public LstCelularModel()
        {
            Registros = new List<LstCelularModel>();
            LstDetalles = new List<LstCelularDetalleModel>();
        }

        #region variables
        public string MobileLineId { get; set; }
        public string MobileLine { get; set; }
        public string Description { get; set; }
        public List<LstCelularModel> Registros { get; set; }
        public List<LstCelularDetalleModel> LstDetalles { get; set; }
        #endregion

        /// <summary>
        /// Obtener Registros Celulares
        /// </summary>
        /// <returns></returns>
        public List<LstCelularModel> ObtenerRegistros()
        {
            using (var bd = new Contexto())
            {
                List<LstCelularModel> listaMapeada = new List<LstCelularModel>();
                var repositorioCelular = new CelularesRepositorio(bd);
                
                foreach (var item in repositorioCelular.ListaCelulares())
                {
                    var temp = new LstCelularModel
                    {
                        MobileLineId = item.MobileLineId.ToString(),
                        MobileLine = item.MobileLine.ToString(),
                        Description = item.Description
                    };
                    listaMapeada.Add(temp);
                }
                return listaMapeada;
            }
        }

        /// <summary>
        /// Guardar listado de archivo importado
        /// </summary>
        public void Guardar()
        {
            using (var bd = new Contexto())
            {
                var repositorioCelular = new CelularesRepositorio(bd);
                var registroGuardar = new List<ListaCelulares>();
                foreach (var item in Registros)
                {
                    var temp = new ListaCelulares
                    {
                        MobileLine = Convert.ToInt64(item.MobileLine),
                        Description = item.Description
                    };
                    registroGuardar.Add(temp);
                }
                repositorioCelular.GuardarLista(registroGuardar);
            }
        }
    }
}