﻿using Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InterCel.Models
{
    public class LoginModel
    {
        #region variables
        public string Login { get; set; }
        public string Contrasena { get; set; }
        #endregion

        /// <summary>
        /// Verificar accesso de usuario
        /// </summary>
        /// <param name="log">Login</param>
        /// <param name="contra">Contraseña</param>
        /// <returns>bool</returns>
        public bool LoginUsuario(string log, string contra)
        {
            using (var bd = new Contexto())
            {
                if (bd.Usuario.Where(x => x.Login == log && x.Contrasena == contra).FirstOrDefault() == null)
                    return false;
                return true;
            }
        }
    }
}