﻿using Datos;
using Datos.Celulares;
using System;
using System.Collections.Generic;
using System;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Web.Services;

namespace InterCel.Models
{
    public class LstCelularDetalleModel
    {
        /// <summary>
        /// Constructor Modelos
        /// </summary>
        public LstCelularDetalleModel()
        {
            LstDetalles = new List<LstCelularDetalleModel>();
        }

        #region variables
        public string CallDetailId { get; set; }
        public string MobileLine { get; set; }
        public string CalledPartyNumber { get; set; }
        public string CalledPartyDescription { get; set; }
        public DateTime DateTime { get; set; }
        public int DurationComplete { get; set; }
        public TimeSpan Duration { get; set; }
        public int? TotalCost { get; set; }
        public string Description { get; set; }
        public List<LstCelularDetalleModel> LstDetalles { get; set; }
        #endregion

        /// <summary>
        /// Detalle de listado de celulares
        /// </summary>
        /// <param name="id">MobileLine</param>
        /// <returns></returns>
        public List<LstCelularDetalleModel> ObtenerRegistros(int id)
        {
            using (var bd = new Contexto())
            {
                List<LstCelularDetalleModel> listaMapeada = new List<LstCelularDetalleModel>();
                var repositorioCelular = new CelularesRepositorio(bd);
                try
                {
                    var lstCelularDetalle = repositorioCelular.ListaCelularesDetalle(id);

                    foreach (var item in lstCelularDetalle.Detalle)
                    {
                        var temp = new LstCelularDetalleModel
                        {
                            Description = lstCelularDetalle.Description,
                            MobileLine = item.MobileLine.ToString(),
                            CalledPartyNumber = item.CalledPartyNumber.ToString(),
                            CalledPartyDescription = item.CalledPartyDescription,
                            DateTime = (DateTime)((item.DateTime != null) ? item.DateTime : DateTime.Now),
                            Duration = (TimeSpan)((item.Duration != null) ? TimeSpan.FromMinutes(Convert.ToInt32(item.Duration / 60)) : TimeSpan.FromMinutes(0)),
                            TotalCost = item.TotalCost
                        };
                        listaMapeada.Add(temp);
                    }
                }
                catch (Exception ex)
                {
                    var a = ex.Message;
                }
                return listaMapeada;
            }
        }

        /// <summary>
        /// Guardar lista de detalles importada
        /// </summary>
        public void Guardar()
        {
            using (var bd = new Contexto())
            {
                var repositorioCelularDetalle = new CelularesDetalleRepositorio(bd);
                var registroGuardar = new List<Detalle>();
                foreach (var item in LstDetalles)
                {

                    var temp = new Detalle
                    {
                        MobileLine = Convert.ToInt64(item.MobileLine),
                        CalledPartyNumber = item.CalledPartyNumber,
                        CalledPartyDescription = item.CalledPartyDescription,
                        DateTime = item.DateTime,
                        Duration = item.DurationComplete,
                        TotalCost = item.TotalCost
                    };
                    registroGuardar.Add(temp);
                }
                repositorioCelularDetalle.GuardarDetalle(registroGuardar);

            }
        }
    }
}