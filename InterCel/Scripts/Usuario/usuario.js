﻿var UsuarioController = function () {
    var urls;
    var modelUsuario = {};
    var idUsuario = 0;

    function Inicio() {

        $(document).on("click", "#btnModalUsuario", function () {
            $("#modalUsuarioTitulo").text("Nuevo Usuario");
            $("#modalUsuarios").modal("toggle");
            $("#guardarUsuario").show();
            $("#modificarUsuario").hide();
        });

        $(document).on("click", "#guardarUsuario", function () {
            ModeloUsuario();
            if (ValidarModelo()) {
                GuardarUsuario();
            }
        }); 

        $(document).on("click", "#modificarUsuario", function () {
            ModeloUsuario();
            modelUsuario.IDUsuario = idUsuario;
            if (ValidarModelo()) {
                ModificarUsuario(idUsuario);
            }
        });

        $(document).on("click", ".btnModificar", function () {
            $("#modalUsuarioTitulo").text("Modificar Usuario");
            $("#guardarUsuario").hide();
            $("#modificarUsuario").show();
            idUsuario = parseInt(this.id);
            ObtenerUsuario(this.id);
        });
        $(document).on("click", ".btnEliminar", function () {
            EliminarUsuario(parseInt(this.id));
        });
    }

    function ModeloUsuario() {
        modelUsuario.IDTipoUsuario = $("#selectTipoUsuario").val();
        modelUsuario.IDRol = $("#selectRol").val();
        modelUsuario.Nombre = $("#nombre").val();
        modelUsuario.Login = $("#login").val();
        modelUsuario.Contrasena = $("#contrasena").val();
        return modelUsuario;
    }

    function LlenarModeloUsuario(data) {
        $("#selectTipoUsuario").val(data.IDTipoUsuario);
        $("#selectRol").val(data.IDRol);
        $("#nombre").val(data.Nombre);
        $("#login").val(data.Login);
        $("#contrasena").val(data.Contrasena);
    }

    function LimpiarModelo() {
        //Elementos
        $("#selectTipoUsuario").val("0");
        $("#selectRol").val("0");
        $("#nombre").val("");
        $("#login").val("");
        $("#contrasena").val("");
        //Modelo
        modelUsuario.IDUsuario = 0;
        modelUsuario.IDTipoUsuario = "0";
        modelUsuario.IDRol = "0";
        modelUsuario.Nombre = "";
        modelUsuario.Login = "";
        modelUsuario.Contrasena = "";
    }

    function GuardarUsuario() {
        $.post(urls.guardar, { usuario: modelUsuario }, function (response) {
            $("#modalUsuarios").modal("toggle");
            alert(response);
        }).fail(function (xhr, status, error) {
            alert('Falla de conexión');
        }).done(function () {
            $("#asignarPartialUsuario").load(urls.lista, function () {
                RestartDataTable($tblLstUsuarios);
                InitDataTable('#tblLstUsuarios');
            });
        });
        LimpiarModelo();
    }

    function ModificarUsuario(id) {
        
        $.post(urls.modificar, { usuario: modelUsuario }, function (response) {
            $("#modalUsuarios").modal("toggle");
            alert(response);
        }).fail(function (xhr, status, error) {
            alert('Falla de conexión');
        }).done(function () {
            $("#asignarPartialUsuario").load(urls.lista, function () {
                RestartDataTable($tblLstUsuarios);
                InitDataTable('#tblLstUsuarios');
            });
        });
        LimpiarModelo();
    }

    function EliminarUsuario(id) {
        $.post(urls.eliminar, { idUsuario: id }, function (response) {
            alert(response);
        }).fail(function (xhr, status, error) {
            alert('Falla de conexión');
        }).done(function () {
            $("#asignarPartialUsuario").load(urls.lista, function () {
                RestartDataTable($tblLstUsuarios);
                InitDataTable('#tblLstUsuarios');
            });
        });
        
        LimpiarModelo();
    }

    function ObtenerUsuario(id) {
        $.post(urls.obtenerUsuario, { idUsuario: parseInt(id) }, function (response) {
            $("#modalUsuarios").modal("toggle");
            LlenarModeloUsuario(response.data);
        }).fail(function (xhr, status, error) {
            alert('Falla de conexión');
        }).done(function () {

        });
        modelUsuario.IDUsuario = 0;
        LimpiarModelo();
    }

    function ValidarModelo() {
        var flag = true;
        $.each(modelUsuario, function (mod, index) {
            switch (mod) {
                case 'IDTipoUsuario':
                    if (index == "0") {
                        alert("Error: Seleccione tipo de usuario");
                        return flag = false;
                    }
                    break;
                case 'IDRol':
                    if (index == "0") {
                        alert("Error: Seleccione tipo de rol");
                        return flag = false;
                    }
                    break;
                default:
                    if (index == "") {
                        alert("Error: Campos vacios");
                        return flag = false;
                    }
                    break;
            }
        });
        return flag;
    }

    function RestartDataTable(element) {
        element.destroy();
        element.draw();
    }

    function InitDataTable(idElement) {
        $(idElement).DataTable({
            "language": {
                "decimal": "",
                "emptyTable": "SIN DATOS DISPONIBLES",
                "info": "MOSTRANDO _START_ DE _END_ <BR><b>_TOTAL_</b> SOLICITUDES ENCONTRADAS",
                "infoEmpty": "SIN DATOS",
                "infoFiltered": "<BR>FILTRADO DE <b>_MAX_</b> SOLICITUDES TOTALES",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "MOSTRAR _MENU_",
                "loadingRecords": "CARGANDO...",
                "processing": "PROCESANDO ...",
                "search": "BÚSQUEDA GENERAL:",
                "zeroRecords": "BÚSQUEDA SIN DATOS",
                "paginate": {
                    "first": "PRIMERO",
                    "last": "ÚLTIMO",
                    "next": "SIGUIENTE",
                    "previous": "PREVIO"
                },
                "aria": {
                    "sortAscending": ": FILTRAR ASCENDENTE",
                    "sortDescending": ": FILTRAR DESCENDENTE"
                }
            }
        });
    }

    return {
        arranque: function (u) {
            urls = u;
            Inicio();
        }
    }
}