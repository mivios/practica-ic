﻿var CelularController = function () {
    var url;

    function inicio() {
        document.getElementById('formListado').onsubmit = function () {
            var formdata = new FormData();
            var fileInput = document.getElementById('archivoListado');
            if (fileInput.files.length > 0) {
                for (i = 0; i < fileInput.files.length; i++) {
                    formdata.append(fileInput.files[i].name, fileInput.files[i]);
                }
                //Creando XML request
                var xhr = new XMLHttpRequest();
                xhr.open('POST', url.archivo);
                xhr.send(formdata);
                xhr.onreadystatechange = function () {
                    if (xhr.readyState == 4 && xhr.status == 200) {
                        $("#asignarPartial").load(urls.lista, function () {
                            RestartDataTable($tblLstCelular);
                            InitDataTable('#tblLstCelulares');
                        });
                        alert(xhr.responseText);
                    }
                }
                $("#archivoListado").val("");
                return false;
            } else {
                alert("suba archivo");
                return false;
            }
        }

        document.getElementById('formDetalle').onsubmit = function () {
            var formdata = new FormData();
            var fileInput = document.getElementById('archivoDetalle');
            if (fileInput.files.length > 0) {
                for (i = 0; i < fileInput.files.length; i++) {
                    formdata.append(fileInput.files[i].name, fileInput.files[i]);
                }
                //Creando XML request
                var xhr = new XMLHttpRequest();
                xhr.open('POST', url.archivoDetalle);
                MostrarDivCargando("Subiendo Archivos.")
                xhr.send(formdata);
                xhr.onreadystatechange = function () {
                    if (xhr.readyState == 4 && xhr.status == 200) {
                        $("#asignarPartial").load(urls.lista, function () {
                            RestartDataTable($tblLstCelular);
                            InitDataTable('#tblLstCelulares');
                        });
                        CerrarDivCargando();
                        alert(xhr.responseText);
                    }
                }
                $("#archivoDetalle").val("");
                return false;
            } else {
                alert("suba archivo");
                return false;
            }
        }

        $(document).on("click", ".detalleCel", function () {
            ObtenerDetalles(this.id);            
        });
    }

    function RestartDataTable(element) {
        element.destroy();
        element.draw();
    }

    function InitDataTable(idElement) {
        $(idElement).DataTable({
            "language": {
                "decimal": "",
                "emptyTable": "SIN DATOS DISPONIBLES",
                "info": "MOSTRANDO _START_ DE _END_ <BR><b>_TOTAL_</b> SOLICITUDES ENCONTRADAS",
                "infoEmpty": "SIN DATOS",
                "infoFiltered": "<BR>FILTRADO DE <b>_MAX_</b> SOLICITUDES TOTALES",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "MOSTRAR _MENU_",
                "loadingRecords": "CARGANDO...",
                "processing": "PROCESANDO ...",
                "search": "BÚSQUEDA GENERAL:",
                "zeroRecords": "BÚSQUEDA SIN DATOS",
                "paginate": {
                    "first": "PRIMERO",
                    "last": "ÚLTIMO",
                    "next": "SIGUIENTE",
                    "previous": "PREVIO"
                },
                "aria": {
                    "sortAscending": ": FILTRAR ASCENDENTE",
                    "sortDescending": ": FILTRAR DESCENDENTE"
                }
            }
        });
    }

    function MostrarDivCargando(texto) {
        var textoMostrar = texto != null ? texto : "Cargando...";
        var cargandoHtml = "<div id='divCargandoElementos'><img src='/Content/img/gears-anim.gif' alt='Cargando...'/><br /><label class='h1 font-white'>"
            + textoMostrar + "</label></div>";

        $("#divCargando").show();
        $("#divCargando").html(cargandoHtml);
    }

    function CerrarDivCargando() {
        $("#divCargando").hide();
        $("#divCargando").html("");
    }

    function ObtenerDetalles(idCel) {
        $("#asignarPartialDetalle").load(urls.listaDetalles + '?id=' + idCel, function () {
            RestartDataTable($tblLstCelularDetalle);
            InitDataTable('#tblLstCelularesDetalle');
            $("#modalDetalles").modal("toggle");
        });
    }


    return {
        arranque: function (u) {
            url = u;
            inicio();
        }
    }
}