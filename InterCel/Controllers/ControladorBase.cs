﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace InterCel.Controllers
{
    public class ControladorBase : Controller
    {
        /// <summary>
        /// Mapeo para controladores
        /// </summary>
        /// <param name="filterContext">ActionExecution</param>
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var controlador = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;
            var action = filterContext.ActionDescriptor.ActionName;
            var sesion = Session["login"];

            base.OnActionExecuting(filterContext);

            if (sesion == null)
                filterContext.Result = ObtenerRedireccionamiento("Login", "Index");
            
        }

        /// <summary>
        /// Redireccionamiento
        /// </summary>
        /// <param name="controlador"></param>
        /// <param name="accion"></param>
        /// <param name="area"></param>
        /// <returns></returns>
        private static RedirectToRouteResult ObtenerRedireccionamiento(string controlador, string accion, string area = null)
        {
            return new RedirectToRouteResult(new RouteValueDictionary(new
            {
                controller = controlador,
                action = accion,
                area
            }));
        }
    }
}