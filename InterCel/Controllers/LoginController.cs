﻿using InterCel.Models;
using System.Web.Mvc;

namespace InterCel.Controllers
{
    public class LoginController : Controller
    {
        public LoginModel loginModel;

        /// <summary>
        /// Constructor
        /// </summary>
        public LoginController()
        {
            loginModel = new LoginModel();
        }

        /// <summary>
        /// GET: Inicio Login
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            if (Session["login"] == null)
            {
                LoginModel a = new LoginModel();
                ViewBag.Title = "Login";
                return View(a);
            }
            else
            {
                return RedirectToAction("Index", "Celulares");
            }
        }

        /// <summary>
        /// POST: Login
        /// </summary>
        /// <param name="usr">modelo login</param>
        /// <returns>Redirección</returns>
        [HttpPost]
        public ActionResult Index(UsuarioModel usr)
        {
            if (loginModel.LoginUsuario(usr.Login, usr.Contrasena))
            {
                Session["login"] = usr.Login;
                return RedirectToAction("Index", "Celulares");

            }
            else
            {
                ViewBag.Error = "Usuario / Contraseña Incorrecta";
                return View(loginModel);
            }
        }

        /// <summary>
        /// Cerrar sesión de usuarios
        /// </summary>
        /// <returns></returns>
        public ActionResult CerrarSesion()
        {
            Session["login"] = null;
            return RedirectToAction("Index", "Login");
        }
    }
}