﻿using InterCel.Models;
using System;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Web.Services;

namespace InterCel.Controllers
{
    public class CelularesController : ControladorBase
    {
        private LstCelularModel modelo;
        private LstCelularDetalleModel modeloDetalle;

        /// <summary>
        /// Constructor
        /// </summary>
        public CelularesController()
        {
            modelo = new LstCelularModel();
            modeloDetalle = new LstCelularDetalleModel();
        }

        /// <summary>
        /// GET inicio
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            modelo.Registros = modelo.ObtenerRegistros();
            return View(modelo);
        }

        /// <summary>
        /// Listado de celulares default
        /// </summary>
        /// <returns>Vista Parcial</returns>
        public PartialViewResult Lista()
        {
            modelo.Registros = modelo.ObtenerRegistros();
            return PartialView("_LstCelulares", modelo);
        }

        /// <summary>
        /// Detalles de lista de celulares
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Vista Parcial</returns>
        public PartialViewResult ListaDetalles(string id)
        {
            modelo.LstDetalles = modeloDetalle.ObtenerRegistros(Convert.ToInt32(id));
            return PartialView("_modalDetalle", modelo);
        }

        /// <summary>
        /// Importar archivo .cvs, listado celulares
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        [HttpPost]
        public JsonResult Archivo()
        {
            string fileName = "s/n";
            int contador = 0;

            for (int i = 0; i < Request.Files.Count; i++)
            {
                HttpPostedFileBase file = Request.Files[i];
                Stream fileContent = file.InputStream;
                fileName = file.FileName;

                using (var reader = new StreamReader(fileContent))
                {
                    try
                    {
                        while (!reader.EndOfStream)
                        {
                            var line = reader.ReadLine();
                            var values = line.Split(',');
                            if (contador > 0) // Omitir primer rengolon (título).
                            {
                                var flagEmpty = true;
                                foreach (var item in values)// Validar campo vacio, se omite el registro completo
                                {
                                    if (item == "")
                                    {
                                        flagEmpty = false;
                                    }
                                }
                                if (flagEmpty)
                                {
                                    var modeloConstructor = new LstCelularModel();

                                    modeloConstructor.MobileLineId = values[0];
                                    modeloConstructor.MobileLine = values[1];
                                    modeloConstructor.Description = values[2];
                                    modelo.Registros.Add(modeloConstructor);
                                    flagEmpty = false;
                                }
                            }
                            contador++;
                        }
                        modelo.Guardar();
                    }
                    catch (Exception ex)
                    {
                        return Json("Error: " + ex.Message);
                    }
                }
            }
            return Json("Archivo " + fileName + " - Registros:" + contador);
        }

        /// <summary>
        /// Importar archivo .cvs, detalle listado celulares
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        [HttpPost]
        public JsonResult ArchivoDetalle()
        {
            string fileName = "s/n";
            int contador = 0;

            for (int i = 0; i < Request.Files.Count; i++)
            {
                HttpPostedFileBase file = Request.Files[i];
                Stream fileContent = file.InputStream;
                fileName = file.FileName;

                using (var reader = new StreamReader(fileContent))
                {
                    try
                    {
                        while (!reader.EndOfStream)
                        {
                            var line = reader.ReadLine();
                            var values = line.Split(',');
                            if (contador > 0) // Omitir primer rengolon (título).
                            {
                                var flagEmpty = true;
                                for (int f = 0; f < values.Length; f++)
                                {
                                    if(f < 7)
                                    {
                                        if (values[f] == "")
                                        {
                                            flagEmpty = false;
                                        }
                                    }                                    
                                }
                                if (flagEmpty)
                                {
                                    var modeloConstructor = new LstCelularDetalleModel();

                                    modeloConstructor.CallDetailId = values[0];
                                    modeloConstructor.MobileLine = values[1];
                                    double callPartyNumber = 0;
                                    bool cpn = Double.TryParse(values[2], out callPartyNumber);

                                    modeloConstructor.CalledPartyNumber = cpn ? Convert.ToDouble(values[2]).ToString() : "0";
                                    modeloConstructor.CalledPartyDescription = values[3];

                                    var fecha = DateTime.Now;                                    
                                    bool fe = DateTime.TryParse(values[4], out fecha);                                    
                                    modeloConstructor.DateTime = fe ? fecha : DateTime.Now;

                                    int duration = 0;
                                    bool du = Int32.TryParse(values[5], out duration);
                                    modeloConstructor.DurationComplete = du ? duration : 0;

                                    int costo = 0;
                                    Int32.TryParse(values[6], out costo);
                                    modeloConstructor.TotalCost = costo;

                                    modeloDetalle.LstDetalles.Add(modeloConstructor);
                                    flagEmpty = false;
                                }
                            }
                            contador++;
                        }
                        modeloDetalle.Guardar();
                    }
                    catch (Exception ex)
                    {
                        return Json("Error: " + ex.Message);
                    }
                }
            }
            return Json("Archivo " + fileName + " - Registros:" + contador);
        }

    }
}