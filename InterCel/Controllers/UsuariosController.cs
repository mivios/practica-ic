﻿using InterCel.Models;
using System;
using System.Web.Mvc;
using System.Web.Services;

namespace InterCel.Controllers
{
    public class UsuariosController : ControladorBase
    {
        private UsuarioModel modeloUsuario;

        /// <summary>
        /// Constructor
        /// </summary>
        public UsuariosController()
        {
            modeloUsuario = new UsuarioModel();
        }

        /// <summary>
        /// GET: Inicio Usuarios
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            modeloUsuario.Usuarios = modeloUsuario.ObtenerUsuarios();
            return View(modeloUsuario);
        }

        /// <summary>
        /// Vista Parcial, listado de usuarios
        /// </summary>
        /// <returns>vista parcial</returns>
        public PartialViewResult Lista()
        {
            modeloUsuario.Usuarios = modeloUsuario.ObtenerUsuarios();
            return PartialView("_LstUsuarios", modeloUsuario);
        }

        /// <summary>
        /// Guardar usuario
        /// </summary>
        /// <param name="usuario">modelo mapeado de usuarioModel</param>
        /// <returns></returns>
        [WebMethod]
        [HttpPost]
        public JsonResult Guardar(UsuarioModel usuario)
        {
            try
            {
                modeloUsuario.Guardar(usuario);
                return Json("Usuario Guardado Correctamente");
            }
            catch (Exception ex)
            {
                return Json("Error: " + ex.Message);
            }
        }

        /// <summary>
        /// Modificar usuario
        /// </summary>
        /// <param name="usuario">modelo mapeado para modificar de usuarioModel</param>
        /// <returns></returns>
        [WebMethod]
        [HttpPost]
        public JsonResult Modificar(UsuarioModel usuario)
        {
            try
            {
                modeloUsuario.Modificar(usuario);
                return Json("Usuario Modificado Correctamente");
            }
            catch (Exception ex)
            {
                return Json("Error: " + ex.Message);
            }
        }

        /// <summary>
        /// Eliminar usuario
        /// </summary>
        /// <param name="idUsuario">Identificador de usuario</param>
        /// <returns></returns>
        [WebMethod]
        [HttpPost]        
        public JsonResult Eliminar(int idUsuario)
        {
            try
            {
                modeloUsuario.Eliminar(idUsuario);
                return Json("Usuario Eliminado Correctamente");
            }
            catch (Exception ex)
            {
                return Json("Error: " + ex.Message);
            }
        }

        /// <summary>
        /// Obtener detalles de usuario
        /// </summary>
        /// <param name="idUsuario">Identificador de usuario</param>
        /// <returns></returns>
        [WebMethod]
        [HttpPost]
        public JsonResult ObtenerUsuario(int idUsuario)
        {
            try
            {
                modeloUsuario.Usuario = modeloUsuario.ObtenerUsuario(idUsuario);
                return Json(new
                {
                    data = modeloUsuario.Usuario
                });
            }
            catch (Exception ex)
            {
                return Json("Error: " + ex.Message);
            }

        }
    }
}